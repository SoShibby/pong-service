package com.mittvast.pongservice;

import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PongController {

    @RequestMapping(method = RequestMethod.GET)
    public String test(ModelMap model) {
        return "pong";
    }

}
