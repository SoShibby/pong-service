# Pong Service

## Docker
Package the "pong-service" into a jar file:
```
mvnw clean package
```

Build the docker image:
```
docker build -t pong-service .
```

Run the pong-service in docker:
```
docker run -p 8080:8080 -t pong-service
```
